import unittest
import requests


class MyTestCase(unittest.TestCase):
    def test_submit_none_params(self):
        data = {
        }
        result = requests.post("http://localhost:5000/new", data).json()
        self.assertEqual(result["success"], False)

    def test_submit(self):
        data = {
            "distance": "200",
            "date": "2020-01-01",
            "time": "10:00",
            "control_time_list": "",
        }
        result = requests.post("http://localhost:5000/new", data).json()
        self.assertEqual(result["success"], False)

    def test_display(self):
        result = requests.get("http://localhost:5000/json-brevet").json()
        self.assertEqual(len(result) > 0, True)


if __name__ == '__main__':
    unittest.main()
